from fuzzy_logic.fuzzy_system import RELEVANCE_INFERENCE, SENTIMENT_INFERENCE

POLARITY = [
    dict(Customers_polarity=0.0,
         Employees_polarity=0.0,
         Text_relevance=0.50,
         Timing=0.50,
         Voting=0),
    dict(Customers_polarity=-0.5,
         Employees_polarity=0.0,
         Text_relevance=0.50,
         Timing=0.50,
         Voting=0),
    dict(Customers_polarity=-0.5,
         Employees_polarity=0.5,
         Text_relevance=0.50,
         Timing=0.50,
         Voting=0),
    dict(Customers_polarity=0.5,
         Employees_polarity=0.0,
         Text_relevance=0.50,
         Timing=0.50,
         Voting=0),
    dict(Customers_polarity=0.5,
         Employees_polarity=-0.5,
         Text_relevance=0.50,
         Timing=0.50,
         Voting=0),
    dict(Customers_polarity=-0.5,
         Employees_polarity=-0.5,
         Text_relevance=0.50,
         Timing=0.50,
         Voting=0),
]

TEXT_RELEVANCE = [
    dict(Customers_polarity=0.0,
         Employees_polarity=0.0,
         Text_relevance=0.1,
         Timing=0.50,
         Voting=0),
    dict(Customers_polarity=0.0,
         Employees_polarity=0.0,
         Text_relevance=0.25,
         Timing=0.50,
         Voting=0),
    dict(Customers_polarity=0.0,
         Employees_polarity=0.0,
         Text_relevance=0.50,
         Timing=0.50,
         Voting=0),
    dict(Customers_polarity=0.0,
         Employees_polarity=0.0,
         Text_relevance=0.75,
         Timing=0.50,
         Voting=0),
    dict(Customers_polarity=0.0,
         Employees_polarity=0.0,
         Text_relevance=0.9,
         Timing=0.50,
         Voting=0),
]

TIMING = [
    dict(Customers_polarity=0.0,
         Employees_polarity=0.0,
         Text_relevance=0.5,
         Timing=0.3,
         Voting=0),
    dict(Customers_polarity=0.0,
         Employees_polarity=0.0,
         Text_relevance=0.5,
         Timing=0.1,
         Voting=0),
    dict(Customers_polarity=0.0,
         Employees_polarity=0.0,
         Text_relevance=0.5,
         Timing=0.015,
         Voting=0),
    dict(Customers_polarity=0.0,
         Employees_polarity=0.0,
         Text_relevance=0.5,
         Timing=0.01,
         Voting=0),
]
VOTING = [
    dict(Customers_polarity=0.0,
         Employees_polarity=0.0,
         Text_relevance=0.5,
         Timing=0.3,
         Voting=-4),
    dict(Customers_polarity=0.0,
         Employees_polarity=0.0,
         Text_relevance=0.5,
         Timing=0.3,
         Voting=-2),
    dict(Customers_polarity=0.0,
         Employees_polarity=0.0,
         Text_relevance=0.5,
         Timing=0.3,
         Voting=0),
    dict(Customers_polarity=0.0,
         Employees_polarity=0.0,
         Text_relevance=0.5,
         Timing=0.3,
         Voting=1),
    dict(Customers_polarity=0.0,
         Employees_polarity=0.0,
         Text_relevance=0.5,
         Timing=0.3,
         Voting=3),
]


def test_mandamy_inference(examples: list, verbose=False):
    for i, case in enumerate(examples):
        SENTIMENT_INFERENCE.set_variable('Customers_polarity', case['Customers_polarity'])
        SENTIMENT_INFERENCE.set_variable('Employees_polarity', case['Employees_polarity'])
        inference = SENTIMENT_INFERENCE.Mamdani_inference()
        RELEVANCE_INFERENCE.set_variable('Sentiment_relevance', inference['Sentiment_relevance'])
        RELEVANCE_INFERENCE.set_variable('Text_relevance', case['Text_relevance'])
        RELEVANCE_INFERENCE.set_variable('Timing', case['Timing'])
        RELEVANCE_INFERENCE.set_variable('Voting', case['Voting'])
        inference = RELEVANCE_INFERENCE.Mamdani_inference(verbose=verbose)
        print(f'example {i} => {inference}')


if __name__ == '__main__':
    # print('*** Effect of polarity on relevance ***')
    # test_mandamy_inference(POLARITY, verbose=False)
    # print('\n*** Effect of text relevance on relevance ***')
    # test_mandamy_inference(TEXT_RELEVANCE, verbose=False)
    print('\n*** Effect of timing on relevance ***')
    test_mandamy_inference(TIMING, verbose=False)
    print('\n*** Effect of voting on relevance ***')
    test_mandamy_inference(VOTING, verbose=False)
