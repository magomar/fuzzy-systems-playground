from simpful import InvSigmoidFuzzySet, SigmoidFuzzySet, FuzzySystem, LinguisticVariable, AutoTriangle

BAD = InvSigmoidFuzzySet(c=0.025, a=32, term='Bad')
GOOD = SigmoidFuzzySet(c=0.025, a=32, term='Good')
OUTPUT = AutoTriangle(5, terms=['XS', 'S', 'M', 'L', 'XL'], universe_of_discourse=[0, 1])
Input = LinguisticVariable([BAD, GOOD], universe_of_discourse=[0, 1])

Rules = ['IF (Input IS Bad) THEN (Output IS XS)',
         'IF (Input IS Good) THEN (Output IS XL)']

FS = FuzzySystem(show_banner=False)
FS.add_linguistic_variable('Input', Input)
FS.add_linguistic_variable('Output', OUTPUT)
FS.add_rules(Rules)
FS.set_variable('Input', 0.3)
result = FS.Mamdani_inference(verbose=True)
print(f'Output={result}')
FS.set_variable('Input', 0.1)
result = FS.Mamdani_inference()
print(f'Output={result}')
FS.set_variable('Input', 0.01)
result = FS.Mamdani_inference()
print(f'Output={result}')
FS.set_variable('Input', 0.001)
result = FS.Mamdani_inference()
print(f'Output={result}')
