from abc import abstractmethod
from dataclasses import InitVar, dataclass, field
from enum import Enum

import numpy as np
from simpful import AutoTriangle, FuzzySet, GaussianFuzzySet, InvSigmoidFuzzySet, LinguisticVariable, SigmoidFuzzySet


@dataclass
class FuzzyDescriptor(LinguisticVariable):
    fuzzy_set_list: InitVar[list[FuzzySet]]
    concept: str
    universe_of_discourse: tuple[int, int]
    # linguistic_variable: LinguisticVariable = field(init=False)
    fuzzy_sets: dict[str, FuzzySet] = field(init=False)

    def __post_init__(self, fuzzy_set_list: list[FuzzySet]):
        super().__init__(fuzzy_set_list, self.concept, self.universe_of_discourse)
        # self.linguistic_variable = LinguisticVariable(fuzzy_set_list, self.concept, self.universe_of_discourse)
        self.fuzzy_sets = {fs._term: fs for fs in fuzzy_set_list}

    def fuzzy_set(self, term: str) -> FuzzySet:
        return self.fuzzy_sets[term]

    @classmethod
    def from_auto_triangle(cls, terms: list[str], concept: str,
                           universe_of_discourse: tuple[int, int]) -> 'FuzzyDescriptor':
        auto_triangle = AutoTriangle(len(terms), terms, universe_of_discourse)
        return cls(auto_triangle._FSlist, concept, universe_of_discourse)

    def get_descriptor(self, crisp_value: float) -> str:
        membership = self.get_values(crisp_value)
        keys = list(membership.keys())
        values = list(membership.values())
        term = keys[np.argmax(values)]
        return term


class EnumFuzzySet(Enum):

    @classmethod
    @abstractmethod
    def universe_of_discourse(cls) -> tuple[int, int]:
        raise NotImplementedError

    @classmethod
    def get_fuzzy_descriptor(cls, concept: str = None) -> FuzzyDescriptor:
        fuzzy_set_list = [x.value for x in cls]
        fuzzy_descriptor = FuzzyDescriptor(fuzzy_set_list, concept or cls.__name__, cls.universe_of_discourse())
        return fuzzy_descriptor


class Polarity(EnumFuzzySet):
    VERY_NEGATIVE = InvSigmoidFuzzySet(c=-0.5, a=8, term='Very_Negative')
    NEGATIVE = GaussianFuzzySet(mu=-0.4, sigma=0.3, term='Negative')
    NEUTRAL = GaussianFuzzySet(mu=0, sigma=0.2, term='Neutral')
    POSITIVE = GaussianFuzzySet(mu=0.4, sigma=0.3, term='Positive')
    VERY_POSITIVE = SigmoidFuzzySet(c=0.5, a=8, term='Very_Positive')

    @classmethod
    def universe_of_discourse(cls) -> tuple[float, float]:
        return -1, 1


class Voting(EnumFuzzySet):
    DOWNVOTED = InvSigmoidFuzzySet(c=-2, a=0.5, term='Downvoted')
    UPVOTED = SigmoidFuzzySet(c=2, a=0.5, term='Upvoted')

    @classmethod
    def universe_of_discourse(cls) -> tuple[float, float]:
        return -5, 5


class TextRelevance(EnumFuzzySet):
    VERY_LOW = InvSigmoidFuzzySet(c=0.25, a=32, term='Very_Low')
    LOW = GaussianFuzzySet(mu=0.25, sigma=0.2, term='Low')
    AVERAGE = GaussianFuzzySet(mu=0.5, sigma=0.2, term='Average')
    HIGH = GaussianFuzzySet(mu=0.75, sigma=0.2, term='High')
    VERY_HIGH = SigmoidFuzzySet(c=0.75, a=16, term='Very_High')

    @classmethod
    def universe_of_discourse(cls) -> tuple[float, float]:
        return 0, 1


class Timing(EnumFuzzySet):
    BAD = InvSigmoidFuzzySet(c=0.03, a=32, term='Bad')
    GOOD = SigmoidFuzzySet(c=0.03, a=32, term='Good')

    @classmethod
    def universe_of_discourse(cls) -> tuple[float, float]:
        return 0, 1


relevance_terms = ['Irrelevant', 'Poorly_Relevant', 'Slightly_Relevant', 'Fairly_Relevant', 'Quite_Relevant',
                   'Really_Relevant', 'Extremely_Relevant']
RELEVANCE7 = FuzzyDescriptor.from_auto_triangle(relevance_terms, 'Relevance', (0, 1))
RELEVANCE5 = FuzzyDescriptor.from_auto_triangle(relevance_terms[1:-1], 'Relevance', (0, 1))
POLARITY = Polarity.get_fuzzy_descriptor()
TEXT_MATCHING = TextRelevance.get_fuzzy_descriptor()
VOTING = Voting.get_fuzzy_descriptor()
TIMING = Timing.get_fuzzy_descriptor()

if __name__ == '__main__':
    # POLARITY.plot()
    VOTING.plot()
    # TEXT_MATCHING.plot()
    # RELEVANCE7.plot()
    # TIMING.plot()
