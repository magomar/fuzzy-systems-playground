SENTIMENT_RULES = [
    'IF (Customers_polarity IS Very_Negative) THEN (Sentiment_relevance IS Extremely_Relevant)',
    'IF (Customers_polarity IS Very_Positive) THEN (Sentiment_relevance IS Really_Relevant)',
    'IF (Employees_polarity IS Very_Negative) THEN (Sentiment_relevance IS Quite_Relevant)',
    'IF (Customers_polarity IS Negative) THEN (Sentiment_relevance IS Fairly_Relevant)',
    'IF (Employees_polarity IS Negative) THEN (Sentiment_relevance IS Slightly_Relevant)'
    'IF (Customers_polarity IS Positive) OR (Employees_polarity IS Very_Positive) '
    'THEN (Sentiment_relevance IS Poorly_Relevant)',
    'IF (Customers_polarity IS Neutral) OR (Employees_polarity IS Positive) '
    'OR (Employees_polarity IS Neutral) THEN (Sentiment_relevance IS Irrelevant)',
]

RELEVANCE_RULES = [
    'IF (Sentiment_relevance IS Extremely_Relevant) THEN (Relevance IS Really_Relevant)',
    'IF (Sentiment_relevance IS Really_Relevant) THEN (Relevance IS Quite_Relevant)',
    'IF (Sentiment_relevance IS Quite_Relevant) THEN (Relevance IS Fairly_Relevant)',
    'IF (Sentiment_relevance IS Fairly_Relevant) THEN (Relevance IS Slightly_Relevant)',
    'IF (Sentiment_relevance IS Slightly_Relevant) THEN (Relevance IS Poorly_Relevant)',
    'IF (Sentiment_relevance IS Poorly_Relevant) OR (Sentiment_relevance IS Irrelevant) THEN (Relevance IS Irrelevant)',

    'IF (Text_relevance IS Very_Low) THEN (Relevance IS Slightly_Relevant)'
    'IF (Text_relevance IS Low) THEN (Relevance IS Fairly_Relevant)',
    'IF (Text_relevance IS Average) THEN (Relevance IS Quite_Relevant)',
    'IF (Text_relevance IS High) THEN (Relevance IS Really_Relevant)',
    'IF (Text_relevance IS Very_High) THEN (Relevance IS Extremely_Relevant)',

    'IF (Timing IS Bad) THEN (Relevance IS Irrelevant)',
    'IF (Timing IS Good) THEN (Relevance IS Extremely_Relevant)',
    'IF (Voting IS Downvoted) THEN (Relevance IS Irrelevant)',
    'IF (Voting IS Upvoted) THEN (Relevance IS Extremely_Relevant)',
]
